#![feature(nll,unboxed_closures)]

use std::path::PathBuf;
pub use crate::file::*;
pub use crate::walker::*;
pub use crate::traits::*; 
pub use crate::socket::{Socket, SendToSocket, ReceiveFromSocket};
pub use crate::command::Command;
pub use std::io::Read;
pub use crate::random::*;
pub mod socket;
pub mod file;
pub mod walker;
pub mod mapping;
pub mod command;
pub mod traits;
pub mod slice;
pub mod random;
pub mod wrapping;

 pub fn main(){
    let mut socket = Socket::create_socket("127.0.0.1".to_string(), "Client".to_string(), 8000).unwrap();
    socket.connect().unwrap();
    let mut buffer = [0u8;512];
    loop{
		let com = Command::Encrypt(PathBuf::from("./silvestri.jpg"),2100);
		println!("COMMAND: {:?}", com);
		com.send_object(&mut socket);
        socket.receive(&mut buffer).unwrap();
        println!("Returned command : {}", String::from_utf8_lossy(&buffer));
		break;
    }
    //let co = "ORCOOODIIOOOOOOOOOOOOOOOO.";
//	let mut seed = RandomSeed::new(2101u32);
//	for i in 0..=20{
//		println!("{}",random(&mut seed))
//	}
//    println!("{:?}",res);
 }
