use std::path::PathBuf;
use std::string::String;
use std::borrow::Cow;

use std::fmt::Write;
use std::usize;

use crate::traits::Serializable;

 #[derive(Debug,PartialEq,Eq)]
 pub enum Command{
    Encrypt(PathBuf,usize),
    Decrypt(PathBuf,usize),
    List,
    ListRec
 }

impl Serializable for Command{
    type Serialized = Command;
    fn serialize(self) -> Option<Cow<'static, str>>{
        match self{
            Command::List =>{
                Some(Cow::from("LSTF"))
            }
            Command::ListRec =>{
                Some(Cow::from("LSTR"))
            }
            Command::Encrypt(path, seed) =>{
                let mut ser = String::new();
                let seeds = format!("{}", seed);
                let pats = path.to_string_lossy();
                ser.push_str("ENCR ");
                match pats{
                    Cow::Borrowed(borr) => {ser.push_str(borr);}
                    Cow::Owned(own) => {ser.push_str(&own);}
                }
                ser.push_str(" ");
                ser.push_str(&seeds);
                Some(Cow::from(ser))
            }
            Command::Decrypt(path,seed) =>{
                let mut ser = String::new();
                let seeds = format!("{}", seed);
                let pats = path.to_string_lossy();
                ser.push_str("DECR ");
                match pats{
                    Cow::Borrowed(borr) => {ser.push_str(borr);}
                    Cow::Owned(own) => {ser.push_str(&own);}
                }
                ser.push_str(" ");
                ser.push_str(&seeds);
                Some(Cow::from(ser))
            }
        }
    }
    fn deserialize(string : &str) -> Option<Self::Serialized>{
		println!("Deserialize   {:?}", string);
        let mut splitter = string.split_terminator(| c: char| c.is_whitespace() || c == '\n');
        let first = splitter.next()?;
        if first == "LSTF"{return Some(Command::List);}
        else if first == "LSTR"{return Some(Command::ListRec);}
        else if first == "ENCR"{
            let last = str::parse::<usize>(splitter.next_back()?);
            if last.is_err(){
                None
            }
            else{
                let string =  splitter.collect::<String>();
                let path = PathBuf::from(string);
                Some(Command::Encrypt(path,last.unwrap()))
            }
        }
        else if first == "DECR"{
            let last = str::parse::<usize>(splitter.next_back()?);
            if last.is_err(){
                None
            }
            else{
                let string =  splitter.collect::<String>();
                let path = PathBuf::from(string);
                Some(Command::Decrypt(path,last.unwrap()))
            }
        }        
        else {return None;}
    }
}



