use std::convert::Into;
use crate::wrapping::*;

pub struct RandomSeed<T : Wrappable<T>>{
	seed : Wrapping<T>
}

impl <T> RandomSeed<T> where T: Wrappable<T>{
	pub fn new(seed : T) -> RandomSeed<T>{
		RandomSeed{ seed : Wrapping(seed) }
	}
	
	fn next(&mut self){	
		self.seed = self.seed * Wrapping(3103515u32.into()) + Wrapping(123456u32.into())  & Wrapping(0x7fff7fffu32.into());
	}
}

pub fn random <T: Wrappable<T>> (seed : &mut RandomSeed<T>) ->  T{
	let _sed = seed.seed;
	seed.next();
	return seed.seed.0;
}