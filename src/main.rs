#![feature(nll,alloc,alloc_system,allocator_api)]
extern crate alloc;
extern crate alloc_system;

#[allow(non_snake_case)]




pub mod conc;

pub use conc::executors::ThreadPoolExecutor;
pub use conc::future::Future;

fn main() {
    println!("Hello, world!");
    let mut exec = ThreadPoolExecutor::new(10);
    let mut vec = Vec::new();
    for i in 0..100{
        vec.push(exec.submit(move ||{println!("{}", i);}).unwrap());
    }
    let mut vec_h = Vec::new();
    let i = loop{
        for (index,fut) in vec.iter().enumerate(){
            if fut.is_done(){
                vec_h.push(index);
            }
        }
        for i in vec_h.drain(..).rev(){
            vec.remove(i);

        }
        if vec.is_empty(){break;}
    }
}
