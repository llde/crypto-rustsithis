//TODO Mutex<> o RwLock<>
use std::sync::{Arc, Mutex,RwLock,mpsc};


pub trait Task<T>  : Send{
    extern "rust-call" fn call(self: Box<Self>) -> T;
}

impl<T,F : Send + FnOnce() -> T> Task<T> for F{
    extern "rust-call" fn call(self : Box<F>) -> T{
        (*self)()
    }
}

#[derive(Copy,Clone)]
enum STATUS{
    INIT,
    SUCCEEDED,
    FAILED,
    RUNNING,
    READED
}


#[derive(Debug)]
pub enum ERRORS{
    AlreadyStarted,
    AlreadyReaded,
    Failed,
    NotStarted,
    NotCompleted,
    Unknow,
}

pub struct Future<T,F>
{
    rec  : RwLock<Option<T>>,
    stat : RwLock<STATUS>,
    funz : RwLock<Option<F>>
}


impl <T,F> Future<T,F>
where T: Send + 'static , F : FnOnce()-> T
{
    pub fn get(&self) -> Result<T,ERRORS>{
        let x = self.stat.read().unwrap().clone();
        if let STATUS::SUCCEEDED = x{
            if let Some(c) = self.rec.write().unwrap().take() {
                *self.stat.write().unwrap() = STATUS::READED;
                Ok(c)
            }
            else{
                panic!();
            }
        }
        else if let STATUS::READED = x{
            Err(ERRORS::AlreadyReaded)
        }
        else if let STATUS::INIT =  x{
            Err(ERRORS::NotStarted)
        }
        else if let STATUS::RUNNING = x{
            Err(ERRORS::NotCompleted)
        }
        else{
            Err(ERRORS::Unknow)
        }
    }

    pub fn is_done(&self) -> bool{
        if let STATUS::SUCCEEDED = *self.stat.read().unwrap(){
            true
        }
        else{
            false
        }
    }


    pub fn run(&self) -> Result<(),ERRORS>{
        if let STATUS::SUCCEEDED = *self.stat.read().unwrap(){
            return Err(ERRORS::AlreadyStarted);
        }
        let funct = self.funz.write().unwrap().take().unwrap();
        *self.stat.write().unwrap() = STATUS::RUNNING;
        let ret = funct();
        *self.rec.write().unwrap() = Some(ret);
        *self.stat.write().unwrap() = STATUS::SUCCEEDED;
        Ok(())
    }

    pub fn new(runnable : F) -> Future<T,F>  {  
        Future{rec:RwLock::new(Option::None) , stat: RwLock::new(STATUS::INIT), funz : RwLock::new(Some(runnable))}
    }
}

unsafe impl <T,F> Send for Future<T,F>{}
unsafe impl <T,F> Sync for Future<T,F>{}
 
