use std::sync::{Arc,Mutex,RwLock};
use std::collections::VecDeque;
use std::thread;

use crate::conc::{Future};


enum STATUS{
    RUNNING,
    SHUTDOWN,
    PAUSED,
}

#[derive(Debug)]
pub enum ERRORS{
    IsShutdown
}

pub struct ThreadPoolExecutor<T,F>{
    workers : Arc<Mutex<VecDeque<Arc<Future<T,F>>>>>,
    status: Arc<Mutex<STATUS>>,
    task_count : u32,
    num_thread: u32,
    current_threads : Arc<RwLock<u32>>,
}
impl <T,F> ThreadPoolExecutor<T,F> where T: Send + 'static, F : FnOnce()-> T +'static {
    //TODO AVOID LOCK POISON
    //TODO TRY TO AVOID THE T PARAMETER
    pub fn new(num_threads : u32) -> ThreadPoolExecutor<T,F>{
        ThreadPoolExecutor{workers: Arc::new(Mutex::new(VecDeque::new())), status : Arc::new(Mutex::new(STATUS::RUNNING)), task_count : 0, num_thread : num_threads, current_threads:Arc::new(RwLock::new(0))}
    }
//TODO innerize Future to avoid Arc
    pub fn submit(&self, function : F ) -> Result<Arc<Future<T,F>>,ERRORS> {
        if let STATUS::SHUTDOWN = *self.status.lock().unwrap(){
            return Err(ERRORS::IsShutdown);
        }
        let fut = Arc::new(Future::new(function));
        self.workers.lock().unwrap().push_back(fut.clone());
      //  println!("{} {}", *self.current_threads.read().unwrap(), self.num_thread);
        if *self.current_threads.read().unwrap() < self.num_thread{
            {
                let mut guard = self.current_threads.write().unwrap();
                *guard += 1;
            }
            let arc = self.workers.clone();
            let arc_curr_thread = self.current_threads.clone();
            thread::spawn(move ||{
                loop{
                    let option;
                    {
                        option = arc.lock().unwrap().pop_front();
                    }
                    if let Some(el) = option{
                        el.run().unwrap();
                    }
                        else{
                            break;
                        }
                }
                *arc_curr_thread.write().unwrap() -= 1;
            });
        }
        Ok(fut)
    }

    pub fn shutdown(&self, now : bool) -> Result<(),ERRORS> {
        if let STATUS::SHUTDOWN = *self.status.lock().unwrap(){
            return Err(ERRORS::IsShutdown);
        }
        *self.status.lock().unwrap() = STATUS::SHUTDOWN;
        if now {
            self.workers.lock().unwrap().clear();
        }
        Ok(())
    }
}

