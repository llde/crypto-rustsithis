
pub fn is_zero(slice : &[u8]) -> bool{
	for i in slice{
		if *i != 0x0u8{
			return false
		}
	}
	true
}

pub fn get_len_until(slice : &[u8], term : u8)  -> usize{
	for (i, byte) in slice.iter().enumerate(){
		if *byte == term{
			return i;
		}
	}
	slice.len()
}