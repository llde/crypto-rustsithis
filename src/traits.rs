use std::borrow::Cow;

pub trait Serializable{
    type Serialized;
	
    fn serialize(self)   -> Option<Cow<'static, str>>;
    fn deserialize(com : &str) -> Option<Self::Serialized>;
}




pub trait WrappingAdd<RHS = Self>{	
	fn wrapping_add(self, other : RHS) -> Self;
}


impl WrappingAdd for u64{
	fn wrapping_add(self, other : Self) -> Self{
		self.wrapping_add(other)
	}
}

impl WrappingAdd for u32{
	fn wrapping_add(self, other : Self) -> Self{
		self.wrapping_add(other)
	}
}

pub trait WrappingMul<RHS  = Self>{	
	fn wrapping_mul(self, other : RHS) -> Self;
}


impl WrappingMul for u64{
	fn wrapping_mul(self, other : Self) -> Self{
		self.wrapping_mul(other)
	}
}

impl WrappingMul for u32{
	fn wrapping_mul(self, other : Self) -> Self{
		self.wrapping_mul(other)
	}
}


pub trait WrappingMod<RHS = Self> {
	fn wrapping_mod(self, other : RHS) -> Self;
}


impl WrappingMod for u32{
	fn wrapping_mod(self, other : Self) -> Self{
		self.wrapping_rem(other)
	}
}



impl WrappingMod for u64{
	fn wrapping_mod(self, other : Self) -> Self{
		self.wrapping_rem(other)
	}
}
