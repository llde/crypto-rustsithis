
extern crate libc;

use std::mem;
use std::ptr;
use std::io;
use std::io::{Write};
use std::ffi::CString;
use std::io::Error;
use std::fmt;

use crate::slice::*;
use crate::traits::Serializable;


use self::libc::AF_INET;
use self::libc::SOCK_STREAM;

extern "C" {
	fn inet_pton(af : libc::c_int, src : *const libc::c_char, dst : *mut libc::c_void) -> libc::c_int;
}

struct Array512<T>{
	pub data : [T;512]
}

impl<T: fmt::Debug> fmt::Debug for Array512<T> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.data[..].fmt(formatter)
    }
}

#[derive(Debug)]
pub enum SocketErrors{
	Io(io::Error),
	InvalidMessage,   //Todo specify errors
}

#[derive(Debug)]
pub enum SocketResult<T>{
	Message(T),
	Terminated,
	Error(SocketErrors)
}

#[derive(Debug)]
pub struct Socket{
    name : String,
    address : String,
	port : u16,
	msg_max_size : u32,
    socket : libc::c_int,
	buffer : Array512<u8>,
	buf_pos : usize,
	buf_len  : usize,
}

pub enum SocketState{
    Closed
}

#[repr(C)]
#[derive(Debug)]
struct in_addrpr {
    s_addr : u32, /* address in network byte order */
}


fn get_os_error() -> SocketErrors{
	SocketErrors::Io(Error::last_os_error())
}


impl Socket{

    pub fn create_socket(address : String, name : String, port : u16) -> io::Result<Socket>{
		let socket = unsafe{libc::socket(AF_INET, SOCK_STREAM, 0)};
		if socket >= 0{
			let buf = Array512{data : [0;512]};
			Ok(Socket{address : address, name:name, port:port, socket : socket, buffer: buf, buf_pos : 0, 
				msg_max_size : 256, buf_len : 0})
		}
		else{
			Err(Error::last_os_error())
		}
    }

    
    pub fn bind(&mut self) -> io::Result<()>{
		let cstring = CString::new(self.address.clone()).unwrap();
		let mut addr = unsafe{mem::zeroed::<libc::in_addr>()};
		let mut saddr = unsafe{mem::zeroed::<libc::sockaddr_in>()};
		let addrptr = &mut addr  as *mut libc::in_addr; 
		let _p = unsafe{inet_pton(AF_INET,cstring.as_bytes_with_nul().as_ptr() as *const i8, addrptr as *mut libc::c_void)};
		unsafe{println!("{:?}", *(addrptr as *const in_addrpr));}
		saddr.sin_port = self.port.to_be(); 
		saddr.sin_family = AF_INET as u16;
		saddr.sin_addr = libc::in_addr{ s_addr : libc::INADDR_ANY};
		let sockaddrin_ptr = &saddr as *const libc::sockaddr_in;
		let size_sockaddr = mem::size_of::<libc::sockaddr_in>() as u32;
        let res = unsafe{libc::bind(self.socket, sockaddrin_ptr as *const libc::sockaddr, size_sockaddr)};
		if  res == 0 { Ok(()) }
		else { Err(Error::last_os_error())}
    }

    pub fn listen(&mut self, backlog : i32) -> io::Result<()> {
		if unsafe{libc::listen(self.socket, backlog)} == 0{
			Ok(())
		}
		else {
			Err(Error::last_os_error())
		}
    }
    
    pub fn accept(&self) -> Socket {  //TODO differenziate between Listener,Connected and AcceptedSocket
        let new_sock = unsafe{libc::accept(self.socket, ptr::null_mut(), ptr::null_mut())};
		let buf = Array512{data : [0;512]};
        Socket{address : self.address.clone(), name : self.name.clone(), port: self.port.clone(),
     		socket : new_sock, buffer : buf, buf_pos : 0, msg_max_size : 256, buf_len :0}
    }
	
	pub fn connect(&mut self) -> io::Result<()>{
		let cstring = CString::new(self.address.clone()).unwrap();
		let mut addr = unsafe{mem::zeroed::<libc::in_addr>()};
		let mut saddr = unsafe{mem::zeroed::<libc::sockaddr_in>()};
		let addrptr = &mut addr  as *mut libc::in_addr; 
		let _p = unsafe{inet_pton(AF_INET,cstring.as_bytes_with_nul().as_ptr() as *const i8, addrptr as *mut libc::c_void)};
		unsafe{println!("{:?}", *(addrptr as *const in_addrpr));}
		saddr.sin_port = self.port.to_be(); 
		saddr.sin_family = AF_INET as u16;
		saddr.sin_addr = libc::in_addr{ s_addr : libc::INADDR_ANY};
		let sockaddrin_ptr = &saddr as *const libc::sockaddr_in;
		let size_sockaddr = mem::size_of::<libc::sockaddr_in>() as u32;
		let conn = unsafe{libc::connect(self.socket, sockaddrin_ptr as *const libc::sockaddr ,size_sockaddr)};
		if  conn == 0 { Ok(()) }
		else { Err(Error::last_os_error())}
	}
	    
	pub fn receive(&mut self, buf: &mut [u8]) -> io::Result<usize>{
	    let recv = unsafe{ libc::recv(self.socket, buf.as_mut_ptr() as *mut libc::c_void, buf.len(), 0)};
		if recv < 0{
			Err(Error::last_os_error())
		}
		else{
			Ok(recv as usize)
		}
	}
	
	
	
	pub fn send(&mut self, buf : &[u8]) -> io::Result<usize>{
		let sent = unsafe{ libc::send(self.socket, buf.as_ptr() as *const libc::c_void, buf.len(), 0)};
		if sent < 0{
			Err(Error::last_os_error())
		}
		else{
			Ok(sent as usize)
		}
	}
}

impl Drop for Socket{
	fn drop(&mut self){
        unsafe{libc::close(self.socket)};
	}
}



pub trait SendToSocket{
	fn send_object(self, sock : &mut Socket) -> SocketResult<usize>;
	
	fn send_message(sock : &mut Socket, msg : &str) -> SocketResult<usize>{
		let _num_send = msg.len();
		let _remainder = msg.len() % sock.msg_max_size as usize;
		let bytes = msg.as_bytes();
		for chunk in bytes.chunks(sock.msg_max_size as usize){
			let mut curr_byte = 0;
			while curr_byte < chunk.len(){
				let res = sock.send(chunk);
				match res{
					Ok(sent) if sent > 0 => {curr_byte+= sent;}
					Ok(_) => {return SocketResult::Terminated;}
					Err(err) => { return SocketResult::Error(SocketErrors::Io(err)) ; }
				}
			}
		}
		let terminator = [0x04u8; 1];
		let res = sock.send(&terminator);	
		match res{
			Ok(sent) if sent > 0 => {SocketResult::Message(msg.len())}
			Ok(_) => {SocketResult::Terminated}
			Err(err) => { SocketResult::Error(SocketErrors::Io(err))}
		}
	}
}


pub trait ReceiveFromSocket{
	type Recevied;
	
	fn recive_object(sock : &mut Socket) -> SocketResult<Option<Self::Recevied>>;
	
	fn recive_message(sock: &mut Socket) -> SocketResult<String>{
		let mut buf : Vec<u8> = Vec::new();
		let mut recv_bytes = 0;
		if sock.buf_len > 0{
			println!("Things in the buffers {} {:?}", sock.buf_len, sock.buffer);
			if sock.buffer.data.contains(&0x04u8){
				println!("Terminator in buffer.");
				let mut clone = sock.buffer.data.clone();
				let mut split = sock.buffer.data.splitn(2,|byte| 	*byte == 0x04u8 );
				let sub = split.next().unwrap();
				buf.extend(sub.into_iter());
				sock.buf_len -= sub.len();
				let sub1 = split.next().unwrap();
				(&mut clone[..]).write_all(sub1).unwrap();
				sock.buf_len += get_len_until(sub1, 0x0u8);
				(&mut sock.buffer.data[..]).write_all(&clone).unwrap();
				match String::from_utf8(buf){
					Ok(stri) =>  {return SocketResult::Message(stri);}
					Err(_) => {return SocketResult::Error(SocketErrors::InvalidMessage);}  //TODO better error conversion
				}
			}
			else{
				buf.extend(sock.buffer.data.into_iter());
				println!("Terminator not in buffer.");
			}
			
		}
		loop{
			sock.buf_len = 0;
			let mut buff = Box::new([0u8;512]);
			let res = sock.receive(&mut buff[..]);
			match res{
				Ok(bytes) if bytes > 0 => {recv_bytes += bytes}
				Ok(_) => {return SocketResult::Terminated;}
				Err(e) => { return SocketResult::Error(SocketErrors::Io(e)); }
			}
			if buff.contains(&0x04u8){
				let split = buff.splitn(2,|byte| *byte == 0x04u8 );
				for (index, sub) in split.enumerate(){
					if index == 0{
						buf.extend(sub.split(|byte|   *byte == 0x0u8).next().unwrap().into_iter());
					}
					else{
						(&mut sock.buffer.data[..]).write_all(sub).unwrap();
						sock.buf_len += get_len_until(sub, 0x0u8);
					}
				}
				break;
			}
			else{
				buf.extend(buff.into_iter());
			}
		}
		let bb = get_len_until(&buf, 0x0u8);
		match String::from_utf8(buf){
			Ok(mut stri) =>  {
				stri.truncate(bb -1);
			    SocketResult::Message(stri)
			}
			Err(_) => {SocketResult::Error(get_os_error())}  //TODO better error conversion
		} 
	}
}




impl <T : Serializable> SendToSocket for T {
	fn send_object(self, sock : &mut Socket) -> SocketResult<usize>{
		let ser = self.serialize().unwrap(); 
		println!("TO SEND : {:?}", ser);		
		Self::send_message(sock, &ser)
	}
}


impl <T> ReceiveFromSocket for T where T : Serializable{
	type Recevied = T::Serialized;	
	fn recive_object(sock : &mut Socket) -> SocketResult<Option<Self::Recevied>>{
		let st = Self::recive_message(sock);
		println!("MESS : {:?}", st);
		match st{
			SocketResult::Terminated => SocketResult::Terminated ,
			SocketResult::Error(err) => SocketResult::Error(err),
			SocketResult::Message(msg) => SocketResult::Message(T::deserialize(&msg)),
		}
	}
}