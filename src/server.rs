#![feature(nll,unboxed_closures)]

pub use crate::file::*;
pub use crate::walker::*;
pub use crate::traits::*; 
pub use crate::socket::{Socket, SendToSocket, ReceiveFromSocket, SocketResult};
pub use libc::c_void;
pub use crate::command::Command;
pub use crate::proto::ProtoMessage;
pub use crate::random::RandomSeed;

pub use crate::conc::executors::ThreadPoolExecutor;
pub use crate::conc::future::Future;

use std::ffi::OsStr;
 
pub mod conc;
pub mod socket;
pub mod file;
pub mod mapping;
pub mod command;
pub mod traits;
pub mod slice;
pub mod proto;
pub mod walker;
pub mod random;
pub mod wrapping;

pub fn xor_execute(_mode : Command) -> bool{
    //match mode {
      //  Crypt(seed) => xor_encrypt(seed) 
        //Decrypt(seed) => xor_decrypt(seed)
    //}
    return false;
}

fn iterate_on_filwalker(walker : SithFileWalker, sock: &mut Socket) {
    for i in walker{
        match i{
            Ok(res) => {res.send_object(sock);}
            Err(_) => {}
        }
    }
}

fn handle_command(com : Command, sock : &mut Socket) -> () {
	println!("Received command: {:?}", com);
	match com {
		Command::List => {
			ProtoMessage::LongOutMode.send_object(sock);
			let walker = SithFileWalker::new("./", false, true);
            iterate_on_filwalker(walker, sock);
			ProtoMessage::ExitOutMode.send_object(sock);
	//		ProtoMessage::TaskSuccess.send_object(&mut sock);
		},
		Command::ListRec => {
			ProtoMessage::LongOutMode.send_object(sock);
			let walker = SithFileWalker::new("./", true, true);
            iterate_on_filwalker(walker, sock);
			ProtoMessage::ExitOutMode.send_object(sock);
		},
		Command::Encrypt(mut path,_seed) => {
			let mut file = SithBuilder::new(&path).access(AccessMode::ReadWrite)
		.		open().unwrap();
		    let trail = path.file_name();
			match trail{
				Some(name) =>{
					let mut owned = name.to_os_string();
					owned.push("_enc");
					path.set_file_name(owned);
				}
				None => {/*TODO send failure */}
			}
			let mut file_enc = SithBuilder::new(&path).access(AccessMode::ReadWrite)
				.open_mode(OpenMode::Create).open_mode(OpenMode::Truncate).user_perm(Perm::All).open().unwrap();
			let _guard = file.lock().unwrap();
			let _guard_enc = file_enc.lock().unwrap();
			file_enc.set_len(file.len().unwrap()).unwrap();
			loop{
				let i = file.map_file(262144);
				let i_enc = file_enc.map_file(262144);
				if let None = i{
					println!("Breaked");
					break;
				}
				let map = i.unwrap().unwrap();
				let mut map_enc = i_enc.unwrap().unwrap();
				let mut val = [42u8;262144];
				let res = map ^ &mut val;
				map_enc.write(&res);
				
			} 

			ProtoMessage::TaskSuccess.send_object(sock);
		},
		Command::Decrypt(mut path,_) => {
			let mut file = SithBuilder::new(&path).access(AccessMode::ReadWrite)
		.		open().unwrap();
			let trail = path.file_name();
			match trail{
				Some(name) =>{
					let mut owned = name.to_os_string();
					let slice_ext = ("_enc".as_ref() as &OsStr).len();
				//	let new_slice = OsStr::new(owned[..(name.len()- slice_ext)]); // TODO Remove the suffix
					path.set_file_name(owned);
				}
				None => {/*TODO send failure */}
			}

			let mut file_enc = SithBuilder::new(&path).access(AccessMode::ReadWrite)
				.open_mode(OpenMode::Create).open_mode(OpenMode::Truncate).user_perm(Perm::All).open().unwrap();
			let _guard = file.lock().unwrap();
			let _guard_enc = file_enc.lock().unwrap();
			file_enc.set_len(file.len().unwrap()).unwrap();
			loop{
				let i = file.map_file(262144);
				let i_enc = file_enc.map_file(262144);
				if let None = i{
					break;
				}
				let map = i.unwrap().unwrap();
				let mut map_enc = i_enc.unwrap().unwrap();
				let mut val = [42u8;262144];
				let res = map ^ &mut val;
				map_enc.write(&res);
			} 
			ProtoMessage::TaskSuccess.send_object(sock);	
		}
	}
}
 
pub fn main(){
    let mut socket = socket::Socket::create_socket("127.0.0.1".to_string(), "Server".to_string(), 8000).unwrap();
    let exec = ThreadPoolExecutor::new(10);
    socket.bind().unwrap();
    socket.listen(32).unwrap();
    println!("Socket listening on 127.0.0.1:8000. Waiting for connection");
    loop{
		let mut socket_acc = socket.accept();
		println!("{:?}", socket_acc);
		let _res = exec.submit(move || {
			ProtoMessage::ServerReady.send_object(&mut socket_acc);
			println!("Accepted connection. Waiting for data.");
			loop{
				let received = Command::recive_object(&mut socket_acc);        
				match received{
					SocketResult::Message(stri) =>{
						match stri {
							Some(cont) => handle_command(cont,&mut socket_acc),
							None => {ProtoMessage::InvalidComm.send_object(&mut socket_acc);}
						}
					} 
					SocketResult::Terminated =>{
						println!("Socket closed by peer");
						return;
					}
					SocketResult::Error(error) => {
						println!("[FATAL] Unexpected Error {:?}. Quitting.", error);
						return;
					}
				}
			}
		});	
    }
 }
	
