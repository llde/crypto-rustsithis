use std::path::{Path};
use std::io::{Error, Result,Read,Write};
use std::ffi::{CString};
use std::convert::AsRef;
use std::mem;
use std::ptr;

use crate::mapping::MemoryMap;


const SITH_RDLOCK : i16 = 0;
const SITH_WRLOCK : i16 = 1;
const SITH_UNLOCK : i16 = 2;

const SITH_READ : i32 = libc::O_RDONLY;
const SITH_WRITE : i32 = libc::O_WRONLY;
const SITH_RW : i32 = libc::O_RDWR;
const SITH_CREATE : i32 = libc::O_CREAT;
const SITH_TRUNCATE : i32 = libc::O_TRUNC;
const SITH_APPEND : i32 = libc::O_APPEND;

const SITH_USERREAD : libc::mode_t = libc::S_IRUSR;
const SITH_USERWRITE : libc::mode_t = libc::S_IWUSR;
const SITH_USEREXEC : libc::mode_t = libc::S_IXUSR;
const SITH_USERALL : libc::mode_t = libc::S_IRWXU;
//TODO group and other modes


pub enum OpenMode{
    Create,
    Truncate,
    Append
}

pub enum AccessMode{
    Read,
    Write,
    ReadWrite
}
pub enum Perm{
    Read,
    Write,
    Execute,
    All
}


pub struct SithBuilder<T : AsRef<Path>>{
    file : T,
    access : Option<i32>,
    openmode : Option<i32>,
    user_perm : Option<libc::mode_t>,
    group_perm : Option<libc::mode_t>,
    other_perm : Option<libc::mode_t>
}

impl <T : AsRef<Path>> SithBuilder<T>{
    pub fn new(path : T) -> SithBuilder<T>{
        SithBuilder{
            file : path, 
            access : None,
            openmode : None,
            user_perm : None,
            group_perm : None,
            other_perm : None
        }
    }

    pub fn access(mut  self, acc : AccessMode) -> SithBuilder<T>{
        match acc{
            AccessMode::Read => self.access = Some(SITH_READ),
            AccessMode::Write => self.access = Some(SITH_WRITE),
            AccessMode::ReadWrite => self.access = Some(SITH_RW),
        }
        self
    }

    pub fn user_perm(mut self, perm : Perm) -> SithBuilder<T>{
        if let Some(_) = self.user_perm {
            match perm{
                Perm::All => self.user_perm = self.user_perm.map(|x| x | SITH_USERALL),
                Perm::Execute => self.user_perm = self.user_perm.map(|x| x| SITH_USEREXEC),
                Perm::Read => self.user_perm = self.user_perm.map(|x| x | SITH_USERREAD),
                Perm::Write => self.user_perm = self.user_perm.map(|x| x | SITH_USERWRITE)
            }
        }
        else{
            match perm{
                Perm::All => self.user_perm = Some(SITH_USERALL),
                Perm::Execute => self.user_perm = Some(SITH_USEREXEC),
                Perm::Read => self.user_perm = Some(SITH_USERREAD),
                Perm::Write => self.user_perm = Some(SITH_USERWRITE)
            }
        }
        self
    }

    pub fn open_mode(mut self, mode : OpenMode) -> SithBuilder<T>{
        if let Some(_) = self.openmode {
            match mode{
                OpenMode::Create => self.openmode = self.openmode.map(|x| x | SITH_CREATE),
                OpenMode::Append => self.openmode = self.openmode.map(|x| x| SITH_APPEND),
                OpenMode::Truncate => self.openmode = self.openmode.map(|x| x | SITH_TRUNCATE),
            }
        }
        else{
            match mode{
                OpenMode::Create => self.openmode = Some(SITH_CREATE),
                OpenMode::Append => self.openmode = Some(SITH_APPEND),
                OpenMode::Truncate => self.openmode = Some(SITH_TRUNCATE),
            }
        }
        self
    }

    pub fn open(self) -> Result<SithFile>{
        println!("{:?}", CString::new(self.file.as_ref().to_str().unwrap()).unwrap());
        let mut mode = 0;
        let mut flags = 0;
        if let Some(flag) = self.access{
            flags = flag;
        }
        else{
            flags = SITH_READ;
        }
        if let Some(flag) = self.openmode{
            flags |= flag;
        }
        if let Some(mo) = self.user_perm{ 
            mode |= mo;
        }
        if let Some(mo) = self.group_perm{
            mode |= mo;
        }
        if let Some(mo) = self.other_perm{
            mode |= mo;
        }
        SithFile::open(self.file, mode, flags)
    }

}

pub struct SithFile{
    file : libc::c_int,  //descriptor or handle typedef.
    offs : u64
}

pub struct SithFileGuard{
    desc : libc::c_int,
    lock_st : libc::flock
}

impl Drop for SithFileGuard{
    fn drop(&mut self){
        self.lock_st.l_type = SITH_UNLOCK;
        unsafe{libc::fcntl(self.desc, libc::F_SETLK, &self.lock_st);}
        println!("Lock dropped.");
    }
}

impl SithFile{
    fn open<P : AsRef<Path>>(path : P, mode : libc::mode_t, flags : i32) -> Result<SithFile>{
        let path_b = CString::new(path.as_ref().to_str().unwrap()).unwrap();
        let repr = path_b.as_bytes_with_nul();
        let fd = unsafe{
            libc::open(repr.as_ptr() as *const libc::c_char, flags , mode)
        };
        if fd == -1{
            Err(Error::last_os_error())
        } 
        else{
            Ok(SithFile{file: fd, offs : 0})
        } 
    }

    pub fn delete<P: AsRef<Path>>(path : P) -> Result<()>{
        let strt = CString::new(path.as_ref().to_str().unwrap()).unwrap();
        let err = unsafe{libc::unlink(strt.as_bytes_with_nul() as *const _ as *const libc::c_char)};
        if err == -1{ Err(Error::last_os_error()) }
        else { Ok(()) }
    }
    
    pub fn len(&self) -> Result<usize>{
        let mut stats  = unsafe{mem::zeroed::<libc::stat>()};
        let res = unsafe{libc::fstat(self.file, &mut stats)};
        if res == -1{ Err(Error::last_os_error()) }
        else {Ok(stats.st_size as usize)}
    }

    pub fn set_len(&mut self, size : usize) -> Result<()>{
        let res = unsafe{libc::ftruncate(self.file,  size as i64)};
        if res == -1{
            Err(Error::last_os_error())
        }
        else{
            Ok(())
        }
    }

    pub fn lock(&mut self) -> Result<SithFileGuard> {
        let lock = libc::flock{
            l_type : SITH_WRLOCK,
            l_whence : libc::SEEK_SET as libc::c_short,
            l_start : 0,
            l_len : 0,
            l_pid : 0
        };
        let error = unsafe{libc::fcntl(self.file, libc::F_SETLKW, &lock)};
        if error == -1 {
            Err(Error::last_os_error())
        }
        else{
            println!("Lock Acquired");
            Ok(SithFileGuard{desc : self.file, lock_st : lock})
        }
    }

    pub fn  map_file(&mut self, mapping_size : usize) -> Option<Result<MemoryMap>>{
        let prot_read_write = libc::PROT_READ | libc::PROT_WRITE;
        let size = self.len().unwrap();
        let mut pagenumber = size / mapping_size;
        let reminder = size % mapping_size;
        let realsize;
        if reminder != 0{
            pagenumber+=1;
        } 
        if self.offs / mapping_size as u64 == (pagenumber as u64 -1){
            realsize = reminder;
        }
        else{
            realsize = mapping_size;
        }
        if  self.offs + mapping_size as  u64 > size as u64{
            if reminder != 0{
                if self.offs + reminder as u64 > size as u64{
                    return None;
                }
            }
            else{
                return None;
            }
        }
        println!("Pages {}  {}    {}  {}", pagenumber,realsize ,reminder, self.offs );
        let addr = unsafe{
            libc::mmap(ptr::null_mut(), mapping_size, prot_read_write,
                libc::MAP_SHARED, self.file, self.offs as i64)
        };
        if addr == libc::MAP_FAILED{
            self.offs += mapping_size as u64;
            Some(Err(Error::last_os_error()))
        }
        else {
            let map = Some(Ok(MemoryMap::create_map(addr, realsize, 0)));
            self.offs += mapping_size as u64;
            return map;
        }
    }

  //  pub fn unlock(){}
    
  //  pub fn close(){}


}

impl Read for SithFile{
    fn read(&mut self, buf : &mut [u8]) -> Result<usize>{
        let buf_length = buf.len();
        let ss = unsafe{libc::read(self.file, buf.as_mut_ptr() as *mut libc::c_void, buf_length)};
        if ss == -1{
            Err(Error::last_os_error())
        }
        else{
            Ok(ss as usize)                
        }
    }

    fn read_to_end(&mut self, buf: &mut Vec<u8>) -> Result<usize>{
        let size = self.len()?;
        buf.reserve(size);
        loop{
            let mut buff = [0; 256];
            let ss = self.read(&mut buff)?;
            if ss < buff.len() {
                buf.extend(buff.into_iter());
                break;
            }
            else if ss == buff.len(){
                buf.extend(buff.into_iter());
            }
        }
        Ok(buf.len())
    }

    fn read_to_string(&mut self, buf: &mut String) ->Result<usize>{
        let size = self.len()?;
        buf.reserve(size);
        loop{
            let mut buff = [0; 256];
            let ss = self.read(&mut buff)?;
            for i in buff.into_iter(){
                buf.push(From::<u8>::from(*i));
            }
            if ss < buff.len() {
                break;
            }
        }
        Ok(buf.len())        
    }
}

impl Write for SithFile{
    fn write(&mut self, buf: &[u8]) -> Result<usize>{
        let buf_length = buf.len();
        let ss = unsafe{libc::write(self.file, buf.as_ptr() as *const libc::c_void, buf_length)};
        if ss == -1{
            Err(Error::last_os_error())
        }
        else{
            Ok(ss as usize)                
        }        
    }

    fn flush(&mut self) -> Result<()>{
        Ok(()) //I'm not buffering the output.
    }

//TODO can I write an arbitrary number of bytes?
    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        for chunk in buf.chunks(256){
            self.write(&chunk)?;
        }
        Ok(())
    }
}

impl Drop for SithFile{
    fn drop(&mut self){
        let succ = unsafe{libc::close(self.file)} == 0;
        println!("SithFile {:?} dropped. {}", self.file, succ);
    }
}
