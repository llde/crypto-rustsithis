use crate::traits::{WrappingAdd, WrappingMul, WrappingMod};
use std::ops::{Add,Mul, BitAnd, Rem};

//trait Wrappable<T> =  WrappingAdd + WrappingMul + WrappingMod +  BitAnd<Output = T> + From<u32>;

pub trait Wrappable<T> : WrappingAdd + WrappingMul + WrappingMod + From<u32> + BitAnd<Output = T> + Clone + Copy{}

impl <T : WrappingAdd + WrappingMul + WrappingMod + From<u32> + BitAnd<Output =T> + Clone + Copy> Wrappable<T> for T{}

#[derive(Clone,Copy)]
pub struct Wrapping<T>(pub T) where T : Wrappable<T>;

impl <T : Wrappable<T>> Add for Wrapping<T>{
	type Output = Self;
	fn add(self, other : Self) -> Self::Output{
		Wrapping(self.0.wrapping_add(other.0))
	}
}

impl <T : Wrappable<T>> Mul for Wrapping<T>{
	type Output = Self;
	fn mul(self, other : Self) -> Self::Output{
		Wrapping(self.0.wrapping_mul(other.0))
	}

}

impl <T : Wrappable<T>> Rem for Wrapping<T>{
	type Output = Self;
	fn rem(self, other : Self) -> Self::Output{
		Wrapping(self.0.wrapping_mod(other.0))
	}
}

impl <T : Wrappable<T>> BitAnd for Wrapping<T>{
	type Output = Self;
	fn bitand(self, other : Self) -> Self::Output{
		Wrapping(self.0 & other.0)
	}
}


impl <T : Wrappable<T>> Add<T> for Wrapping<T>{
	type Output = Self;
	fn add(self, other : T) -> Self::Output{
		Wrapping(self.0.wrapping_add(other))
	}
}

impl <T : Wrappable<T>> Mul<T> for Wrapping<T>{
	type Output = Self;
	fn mul(self, other : T) -> Self::Output{
		Wrapping(self.0.wrapping_mul(other))
	}

}

impl <T : Wrappable<T>> Rem<T> for Wrapping<T>{
	type Output = Self;
	fn rem(self, other : T) -> Self::Output{
		Wrapping(self.0.wrapping_mod(other))
	}
}

impl <T : Wrappable<T>> BitAnd<T> for Wrapping<T>{
	type Output = Self;
	fn bitand(self, other : T) -> Self::Output{
		Wrapping(self.0 & other)
	}
}
