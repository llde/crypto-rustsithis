use std::ops::BitXor;
use std::slice;
use std::mem;

#[derive(Debug)]
pub struct MemoryMap<'a>{
    slice : Option<&'a mut [u8]>,
    offset : usize,
}

//TODO memoryMap with real offset
impl <'a> MemoryMap<'a>{
    pub fn create_map(addr : *mut libc::c_void, page_size : usize, file_offset : u64) -> MemoryMap<'a>{
        let  sl = unsafe{slice::from_raw_parts_mut::<u8>(addr as *mut u8, page_size)};
        let _size = sl.len();
        MemoryMap{slice : Some(sl) , offset : file_offset as usize}
    }

    pub fn write(&mut self, bytes : &[u8]){
        loop{
            self.slice.as_mut().unwrap()[self.offset] =  bytes[self.offset];
            self.offset+=1;
            if self.offset == self.slice.as_ref().unwrap().len(){break;}
            if self.offset == bytes.len(){break;}
        }  
    }
}



impl <'a> BitXor<&'a mut [u8]> for MemoryMap<'a>{
    type Output = Box<[u8]>;
    fn bitxor(mut self, oth : &'a mut [u8]) -> Self::Output{
		let mut vv = vec![0; self.slice.as_ref().unwrap().len()].into_boxed_slice();
        loop{
			vv[self.offset] = self.slice.as_ref().unwrap()[self.offset] ^ oth[self.offset];
            self.offset+=1;
            if self.offset == self.slice.as_ref().unwrap().len(){break;}
            if self.offset == oth.len(){break;}
        }
		vv
    }
}

impl <'a> Drop for MemoryMap<'a>{
    fn drop(&mut self){
		println!("Dropping");
		let slice = self.slice.take().unwrap();
		let sl_ptr = slice.as_mut_ptr();
		let len = slice.len();
        unsafe{libc::munmap(sl_ptr as *mut libc::c_void, len)};
		mem::forget(slice);
		println!("Dropped");
    }
}
