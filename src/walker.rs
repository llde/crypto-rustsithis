extern crate libc;

use std::ptr;
use std::io;
use std::mem;
use std::convert::AsRef;
use std::io::Error;
use std::path::{Path,PathBuf};
use std::ffi::{CStr,CString};


use std::fmt::Write;
use std::borrow::Cow;

pub use crate::traits::*; 

#[derive(Debug)]
pub enum Type {
    File,
    Directory
}

#[derive(Debug)]
pub struct FileEntry {
    file : PathBuf,
    size : i64,
    typ : Type
}


impl Serializable for FileEntry{
    type Serialized = FileEntry;
	//TODO type
    fn serialize(self) -> Option<Cow<'static, str>>{
		let mut buf = String::new();
		let size = format!("{}", self.size);
		buf.write_str(&size).unwrap();
		buf.write_str(" ").unwrap();
		let pats = self.file.to_string_lossy();
		let _res = match pats{
            Cow::Borrowed(borr) => buf.write_str(borr),
            Cow::Owned(own) => buf.write_str(&own),
        };
		Some(Cow::from(buf))
	}
    fn deserialize(_com : &str) -> Option<Self::Serialized>{
		return None;
		//TODO deserialize!
	}
}


#[derive(Debug)]
pub struct SithFileWalker{
    path : PathBuf,
    dir : Option<*mut libc::DIR>,
    recursive : bool,
    nonvalid : bool,
    opt : Option<Box<SithFileWalker>>
}

impl  SithFileWalker{
    pub fn new<T : AsRef<Path>>(path : T, recursive : bool, absolute : bool) -> SithFileWalker{
        if absolute{
            SithFileWalker{path : path.as_ref().canonicalize().unwrap(), 
                dir : None, recursive : recursive, nonvalid: false ,opt: None}
        }
        else{
            SithFileWalker{path : path.as_ref().to_path_buf(), dir : None,
                 recursive : recursive, nonvalid : false ,opt : None}            
        }
    }
}

impl Drop for SithFileWalker{
    fn drop(&mut self){
        if let Some(ptr) = self.dir{
            unsafe{libc::closedir(ptr)};
        }
    }
}

impl Iterator for SithFileWalker{
    type Item = io::Result<FileEntry>;
    fn next(&mut self)-> Option<Self::Item>{
        if self.nonvalid == true{return None;}
        let c_string = CString::new(self.path.to_str().unwrap()).unwrap();
        let bytes = c_string.as_bytes_with_nul();
        if let None = self.dir{
            let dir = unsafe{libc::opendir(bytes as *const _ as *const libc::c_char)};
            if dir == ptr::null_mut(){
                self.nonvalid = true;
                return Some(Err(Error::last_os_error()));
            }
            self.dir = Some(dir);
        }
        let mut remove = false;
        if let Some(ref mut dir) = self.opt{
            let dire = dir.next();
            if let None = dire{
                remove = true;
            }
            else{
                return dire;
            }
        } 
        if remove == true {self.opt.take();}
        let mut file_name;
        let mut dirent = ptr::null_mut();
        let mut b = 0;
        let mut b1 = 0; 
        while  b == 0 || b1 == 0{
            dirent = unsafe{libc::readdir(self.dir.unwrap())};
            if dirent == ptr::null_mut(){return None;}
            file_name = unsafe{(*dirent).d_name};
            b  = unsafe{libc::strcmp(file_name.as_ptr(),b".\0".as_ptr()  as *const libc::c_char)};
            b1 = unsafe{libc::strcmp(file_name.as_ptr(),b"..\0".as_ptr() as *const libc::c_char)};
        }
        let mut new_path = self.path.clone();
        let arr  = unsafe{(*dirent).d_name};
        let mut vec = Vec::new();
        for i in arr.into_iter(){
            vec.push((*i) as u8);
            if *i == 0{break;}
        }
        let ddd = CStr::from_bytes_with_nul(&vec).unwrap();
        new_path.push(ddd.to_string_lossy().into_owned());
        let newstr = CString::new(new_path.to_str().unwrap()).unwrap();
        let new_bytes = newstr.as_bytes_with_nul();
        let  mut stats = unsafe{mem::zeroed::<libc::stat>()};
        let res = unsafe{libc::lstat(new_bytes.as_ptr() as *const i8, &mut stats)};
        if res != 0 { 
            return Some(Err(Error::last_os_error()));
        }
        if (stats.st_mode & libc::S_IFMT) == libc::S_IFDIR{
            if self.recursive == true{
                self.opt = Some(Box::new(SithFileWalker::new(new_path.clone(), self.recursive, false)));
            }
            Some(Ok(FileEntry{file : new_path, size : stats.st_size, typ : Type::Directory}))
        }
        else{
            Some(Ok(FileEntry{file : new_path, size : stats.st_size, typ : Type::File}))
        }
    }
}

