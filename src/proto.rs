use std::borrow::Cow;
use std::string::String;
use std::usize;

use crate::traits::Serializable;

// SERVER RESPONSES
//TODO numeric values also
const SITH_PROTO_ACCEPTED : &'static str = "100";    //Server is ready to accept tasks
    
const SITH_PROTO_SUCCESS : &'static str =     "200";  //Task completed sucessfully
    
const SITH_PROTO_MOREOUT : &'static str =     "300"; //CLient enter long message mode
const SITH_PROTO_MOREEND  : &'static str =    "301"; //Exit long message mode
    
const SITH_PROTO_INVALID : &'static str =     "400"; 
const SITH_PROTO_NOTFOUND : &'static str =    "404";
    
const SITH_PROTO_FAILURE  : &'static str =    "500";
const SITH_PROTO_SERVBUSY : &'static str =    "503";
const SITH_PROTO_NOTIMPL : &'static str =     "542";
    
#[derive(Debug,PartialEq,Eq)]
pub enum ProtoMessage{
	ServerReady,
	TaskSuccess,
	LongOutMode,
	ExitOutMode,
	InvalidComm,
	FailureComm
}

impl Serializable for ProtoMessage{
    type Serialized = ProtoMessage;
    fn serialize(self) -> Option<Cow<'static,str>>{
	    match self{
		//TODO error without ProtoMessage:: . Check latest nightly and file a bug report
            ProtoMessage::ServerReady => Some(Cow::from(SITH_PROTO_ACCEPTED)),
			ProtoMessage::TaskSuccess => Some(Cow::from(SITH_PROTO_SUCCESS)),
			ProtoMessage::LongOutMode => Some(Cow::from(SITH_PROTO_MOREOUT)),
			ProtoMessage::ExitOutMode => Some(Cow::from(SITH_PROTO_MOREEND)),
			ProtoMessage::InvalidComm => Some(Cow::from(SITH_PROTO_INVALID)),
			ProtoMessage::FailureComm => Some(Cow::from(SITH_PROTO_FAILURE)),
			_ => None
		}		
	}
	
    fn deserialize(string : &str) -> Option<Self::Serialized>{
		println!("{:?}", string);
		let prot = str::parse::<usize>(string);
		match prot {
			Ok(num) if num == 100 => Some(ProtoMessage::ServerReady),
			Ok(num) if num == 200 => Some(ProtoMessage::TaskSuccess),
			Ok(num) if num == 300 => Some(ProtoMessage::LongOutMode),
			Ok(num) if num == 301 => Some(ProtoMessage::ExitOutMode),
			Ok(num) if num == 400 => Some(ProtoMessage::InvalidComm),
			Ok(num) if num == 500 => Some(ProtoMessage::FailureComm),
			_ => None
		}
    }
}
